# README #

1/3スケールのSEGA スペースハリアー・シットダウンモデル風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK Fusion360です。

実物は高さ157cmですがモデルは50cmとなっているため、実際には若干小さいものになります。

***

# 実機情報

## メーカ
- セガ・エンタープライゼス

## 発売時期

- 1985年

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/%E3%82%B9%E3%83%9A%E3%83%BC%E3%82%B9%E3%83%8F%E3%83%AA%E3%82%A2%E3%83%BC)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_spaceharrier_sitdown/raw/8c8cf20d61b3016c56254a2fa1b920ac334206cd/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_spaceharrier_sitdown/raw/3fe3566451705c9b4e72432081dd4f45e1cd815b/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_spaceharrier_sitdown/raw/8c8cf20d61b3016c56254a2fa1b920ac334206cd/ModelView_split.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_spaceharrier_sitdown/raw/8c8cf20d61b3016c56254a2fa1b920ac334206cd/ExampleImage.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_spaceharrier_sitdown/raw/8c8cf20d61b3016c56254a2fa1b920ac334206cd/ExampleImage_1.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_spaceharrier_sitdown/raw/8c8cf20d61b3016c56254a2fa1b920ac334206cd/ExampleImage_2.png)
